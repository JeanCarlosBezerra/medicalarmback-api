const User = require('../model/register.model')
const database = require('../config/database')

exports.save = async (data) => {
    let con = await database.connect()
    
    let User = new User(data)
    await User.save()

    con.disconnect()
}