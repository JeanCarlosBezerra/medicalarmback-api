const Login = require('../model/login.model')
const database = require('../config/database')

exports.findByDriver = async (user) => {
    let con = await database.connect()
    let result = await Login.find({ driver: user})

    con.disconnect()
    return result
}