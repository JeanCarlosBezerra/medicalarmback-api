const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    user: mongoose.Types.UserId,
    name: String,
    password: String
})

module.exports = mongoose.model('User', userSchema)