const userRoutes = require('./login.routes')
const userRegister = require('./register.routes')

module.exports = (app) => {
    userRoutes(app)
    userRegister(app)    
}