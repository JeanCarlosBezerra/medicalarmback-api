const repository = require("../repository/login.repository")

module.exports = (app) => {

    app.get('/repository/login.repository/:user', async (req, res) => {
        let user = req.params.user
        let result = await repository.findByDriver(user)
        res.json(result)
    })
}