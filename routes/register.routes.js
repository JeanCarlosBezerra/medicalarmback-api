const repository = require("../repository/user_repository")

module.exports = (app) => {

    app.post('/register', async (req, res) => {
        await repository.save(req.body)
        res.end()
    })
}